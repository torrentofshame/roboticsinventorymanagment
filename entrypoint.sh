#!/bin/bash

cd /home/container

echo "Cloning Repository..."
git clone https://gitlab.com/torrentofshame/roboticsinventorymanagment.git ./app

echo "Moving Files..."
mv ./app/ ./

echo "running setup.py install --user"
python ./setup.py install --user

pip install -r requirements.txt

echo "pip install hook..."
pip install torrentsusefulstuff --no-index -f https://torrentofshame.com/personalpylib/torrentsusefulstuff

python --version

echo "Starting..."

python ./wsgi.py