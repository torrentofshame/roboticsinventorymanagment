import yaml
import os

class Config:
    __cfg = None

    @classmethod
    def __init__(cls, configfile="config.yml"):
        if cls.__cfg is None:
            with open(configfile, 'r') as ymlfile:
                cls.__cfg = yaml.load(ymlfile, Loader=yaml.SafeLoader)

            cls.Sections = cls.__sections

            for section in cls.__cfg:
                cls.add_section_func(section_name=section)

            cls.TESTING = cls.__cfg["webserver"]["TESTING"]
            cls.FLASK_DEBUG = cls.__cfg["webserver"]["FLASK_DEBUG"]
            cls.FLASK_ENV = cls.__cfg["webserver"]["FLASK_ENV"]
            cls.SECRET_KEY = cls.__cfg["webserver"]["SECRET_KEY"]

            cls.SQLALCHEMY_DATABASE_URI = cls.__cfg["database"]["SQLALCHEMY_DATABASE_URI"].replace("%USERPROFILE%", os.path.expanduser("~"))
            cls.SQLALCHEMY_ECHO = cls.__cfg["database"]["SQLALCHEMY_ECHO"]
            cls.SQLALCHEMY_TRACK_MODIFICATIONS = cls.__cfg["database"]["SQLALCHEMY_TRACK_MODIFICATIONS"]

    @classmethod
    def __getitem__(cls, item):
        if cls.__cfg.get(item):
            return cls.__cfg.get(item)
        else:
            return False

    @classmethod
    def __call__(cls, *args, **kwargs):
        return cls.__cfg

    @classmethod
    def __str__(cls):
        return str(cls.__cfg)

    @classmethod
    def __repr__(cls):
        return str(cls.__cfg)

    class __sections:
        __sec = None

        @classmethod
        def __init__(cls):
            if cls.__sec is None:
                cls.__sec = {}

        @classmethod
        def __getitem__(cls, item):
            return cls.__sec[item]

        @classmethod
        def __getattribute__(cls, item):
            return cls.__sec[item]

        @classmethod
        def __setitem__(cls, key, value):
            cls.__sec[key] = value

        @classmethod
        def __setattr__(cls, key, value):
            cls.__sec[key] = value

        @classmethod
        def __delattr__(cls, item):
            del cls.__sec[item]

        @classmethod
        def __call__(cls, *args, **kwargs):
            return cls.__sec

        @classmethod
        def _Config__sec(cls):
            return cls.__sec

        @classmethod
        def __str__(cls):
            return str(cls.__sec)

        @classmethod
        def __repr__(cls):
            return str(cls.__sec)

    @staticmethod
    def add_section_func(section_name):
        class sec_cls:

            def __str__(self):
                return str(Config()[section_name])

            def __repr__(self):
                return str(Config()[section_name])

            @staticmethod
            def __call__(arg=None):
                if arg is None:
                    return Config()[section_name]
                else:
                    return Config()[section_name][arg]

            @staticmethod
            def get(item):
                __conf = Config()[section_name]
                return __conf[item]

            @staticmethod
            def __getitem__(item):
                __conf = Config()[section_name]
                if __conf[item]:
                    return __conf[item]
                else:
                    return False

        capSectionName = section_name.capitalize()

        setattr(Config, capSectionName, sec_cls())

        sec_cls.__name__ = "__" + section_name
        sec_cls.__doc__ = "Return values for the {0} config section".format(section_name)

        Config().Sections()[capSectionName] = getattr(Config(), capSectionName)
