FROM python:3.8.2-alpine

MAINTAINER TorrentofShame, <pterodactyl@torrentofshame.com>

RUN apk add --no-cache --update git bash gcc g++ libffi-dev make \
    && adduser -D -h /home/container container

COPY . /home/container

USER container
ENV USER=container HOME=/home/container

WORKDIR /home/container

EXPOSE 5000

COPY ./entrypoint.sh /entrypoint.sh
CMD ["/bin/bash", "/entrypoint.sh"]