from os import path
from setuptools import setup, find_packages
from io import open

here = path.abspath(path.dirname(__file__))

setup(
    name='Robotics Inventory Managment',
    version='1.0',
    description='Inventory Managment Setup for Robotics Parts',
    url='https://inventory.sbhsengineering.com',
    author='Simon Weizman',
    author_email='t@torrentofshame.com',
    packages=["invmngr_application"],
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        "Click==7.0",
        "cssmin==0.2.0",
        "Flask==1.1.1",
        "Flask-Assets==2.0",
        "Flask-Login==0.5.0",
        "Flask-SQLAlchemy",
        "Flask-Vue==0.3.5",
        "Flask-WTF==0.14.3",
        "itsdangerous==1.1.0",
        "Jinja2==3.0.0a1",
        "MarkupSafe==1.1.1",
        "PyYAML==5.3",
        "SQLAlchemy==1.3.13",
        "webassets==2.0",
        "Werkzeug==1.0.0",
        "WTForms==2.2.1",
    ],
    entry_points={
        'console_scripts': [
            'run = wsgi:app',
            'invmngr = invmngr_application:cli'
        ],
    },
)
