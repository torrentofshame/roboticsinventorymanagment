import click
from flask import current_app as app
from flask.cli import AppGroup, with_appcontext
from .models import User, db

user_cli = AppGroup("user")

@app.cli.command(with_appcontext=False)
def help():
	helpmenu = """\
	This is Help Menu\
	""".format(length='multi-line', ordinal='second')
	print(helpmenu)

@user_cli.command("list")
def list_users():
	q = User.query.order_by(User.id).all()
	for u in q:
		print(u.id, u.name, u.email, u.is_admin(), sep="\t")


@user_cli.command("remove")
@click.argument("uid")
def remove_user(uid):
	uid = int(uid)
	if uid is None: return;
	user_to_remove = User.query.filter_by(id=uid).first()
	if user_to_remove is None: return;
	remuser_name = user_to_remove.name
	db.session.delete(user_to_remove)
	db.session.commit()
	print("User {} has been successfully removed.".format(remuser_name))


@user_cli.command("set_admin")
@click.argument("uid")
def set_admin(uid):
	uid = int(uid)
	if uid is None: return;
	user_to_adminize = User.query.filter_by(id=uid).first()
	if user_to_adminize is None: return;
	if user_to_adminize.is_admin(): return;
	print("Add {} to the admin list in config.yml".format(user_to_adminize.email))
