import hook
from flask import jsonify
from flask import current_app as app
from .models import db, User, Part, Vendor


@hook.Add("bg_gettbl")
def bg_gettbl(tbltoshow: str = ""):
	if tbltoshow == "": return;
	result = hook.Run("get_{}_tbl".format(tbltoshow))
	return result


@hook.Add("get_users_tbl")
def get_users_tbl():
	users = []
	raw_users = User.query.order_by(User.id).all()
	for u in raw_users:
		users.append({
			"id": u.id,
			"name": u.name,
			"email": u.email,
			"website": u.website,
			"admin": u.is_admin(),
			"created_on": u.created_on,
			"last_login": u.last_login
		})
	tblheaders = ["id", "name", "email", "website", "admin", "created_on", "last_login"]
	return jsonify(datatabletitle="Users", tblheaders=tblheaders, tbldata=users)


@hook.Add("get_parts_tbl")
def get_parts_tbl():
	parts = []
	raw_parts = Part.query.order_by(Part.id).all()
	for p in raw_parts:
		parts.append({
			"id": p.id,
			"partnumber": p.partnumber,
			"thumbnail": p.thumbnail,
			"description": p.description,
			"unitprice": p.unitprice,
			"quantity": p.quantity,
			"category": p.category,
			"weblink": p.weblink,
			"vendor": p.vendor
		})
	tblheaders = ["id", "partnumber", "thumbnail", "description", "unitprice", "quantity", "category", "weblink", "vendor"]
	return jsonify(datatabletitle="Parts", tblheaders=tblheaders, tbldata=parts)


@hook.Add("get_vendors_tbl")
def get_vendors_tbl():
	vendors = []
	raw_vendors = Vendor.query.order_by(Vendor.id).all()
	for v in raw_vendors:
		vendors.append({
			"id": v.id,
			"name": v.name,
			"website": v.website
		})
	tblheaders = ["id", "name", "website"]
	return jsonify(datatabletitle="Vendors", tblheaders=tblheaders, tbldata=vendors)


@hook.Add("bg_add_vendor")
def add_vendor(name: str = "", website: str = ""):
	if name == "" or website == "": return False;
	if Vendor.query.filter_by(name=name).first() is not None: return False;
	vendor = Vendor(
		name=name,
		website=website
	)
	db.session.add(vendor)
	didok = db.session.commit()
	return jsonify(didok=didok)


@hook.Add("bg_add_part")
def add_part(partnumber: str = "", description: str = "", unitprice: str = "", quantity: str = "", category: str = "", weblink: str = "", vendor: str = ""):
	if partnumber == "" or description == "" or unitprice == "" or quantity == "" or category == "" or weblink == "" or vendor == "": return jsonify(didok=False);
	if Part.query.filter_by(partnumber=partnumber).first() is not None: return jsonify(didok=False);
	part = Part(
		partnumber=partnumber,
		description=description,
		unitprice=float(unitprice),
		quantity=int(quantity),
		category=category,
		weblink=weblink,
		vendor=vendor,
		thumbnail=None
	)
	db.session.add(part)
	didok = db.session.commit()
	return jsonify(didok=didok)
