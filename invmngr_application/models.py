from config import Config
from . import db
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash

tableprefix = Config().Database()["TABLE_PREFIX"]


class User(UserMixin, db.Model):
	"""DB Model for Users"""

	__tablename__ = tableprefix + "users"

	id = db.Column(
		db.Integer,
		primary_key=True
	)
	name = db.Column(
		db.String,
		nullable=False,
		unique=False
	)
	email = db.Column(
		db.String(40),
		unique=True,
		nullable=False
	)
	password = db.Column(
		db.String(200),
		primary_key=False,
		unique=False,
		nullable=False
	)
	website = db.Column(
		db.String(60),
		index=False,
		unique=False,
		nullable=True
	)
	created_on = db.Column(
		db.DateTime,
		index=False,
		unique=False,
		nullable=True
	)
	last_login = db.Column(
		db.DateTime,
		index=False,
		unique=False,
		nullable=True
	)

	def set_password(self, password):
		"""Create hashed password."""
		self.password = generate_password_hash(password, method='sha256')

	def check_password(self, password):
		"""Check hashed password."""
		return check_password_hash(self.password, password)

	def is_admin(self):
		"""Check if the User is an Admin"""
		return self.email in Config().Admins()


	def __repr__(self):
		return '<User {}>'.format(self.name)


class Vendor(db.Model):
	"""DB Model for Vendors"""

	__tablename__ = tableprefix + "vendors"

	id = db.Column(
		db.Integer,
		primary_key=True
	)
	name = db.Column(
		db.String(100),
		index=False,
		unique=True,
		nullable=False
	)
	website = db.Column(
		db.String(100),
		index=False,
		unique=True,
		nullable=False
	)

	def __repr__(self):
		return "<Supplier {}>".format(self.name)


class Part(db.Model):
	"""DB Model for Parts"""

	__tablename__ = tableprefix + "parts"

	id = db.Column(
		db.Integer,
		primary_key=True
	)
	partnumber = db.Column(
		db.String(20),
		index=False,
		unique=True,
		nullable=False
	)
	thumbnail = db.Column(
		db.String(256),
		index=False,
		unique=False,
		nullable=True
	)
	description = db.Column(
		db.String(256),
		index=False,
		unique=False,
		nullable=False
	)
	unitprice = db.Column(
		db.Float,
		index=False,
		unique=False,
		nullable=False
	)
	quantity = db.Column(
		db.Integer,
		index=False,
		unique=False,
		nullable=True
	)
	category = db.Column(
		db.Integer,
		index=False,
		unique=False,
		nullable=True
	)
	weblink = db.Column(
		db.String(256),
		index=False,
		unique=False,
		nullable=True
	)
	vendor = db.Column(
		db.Integer,
		index=False,
		unique=False,
		nullable=True
	)

	def __repr__(self):
		return "<Part {}>".format(self.name)
