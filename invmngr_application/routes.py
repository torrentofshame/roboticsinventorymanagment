from flask import Blueprint, render_template, redirect, url_for, request, jsonify
from flask_login import login_required, current_user
from .models import Vendor
import hook
import invmngr_application.bg_processes
from flask import current_app as app

main_bp = Blueprint("main_bp", __name__,
					template_folder="templates",
					static_folder="static"
					)


@main_bp.route("/", methods=["GET"])
def dashboard():
	"""Home Page Route"""
	if current_user.is_authenticated:
		if not request.script_root: request.script_root = url_for('main_bp.dashboard', _external=True);

		return render_template(
			'dashboard.jinja2',
			title='Inventory Manager',
			template='dashboard',
			Vendors=Vendor.query.order_by().all()
		)
	else:
		return redirect(url_for("auth_bp.login"))


@main_bp.route("/admin", methods=["GET"])
@login_required
def admin():
	"""Admin Page"""
	if current_user.is_admin():
		return render_template(
				'dashboard.jinja2',
				title='Inventory Manager',
				template='dashboard'
			)
	else:
		return render_template(
			"adminerror.jinja2",
			title="Inventory Manager Admin",

		)


@main_bp.route("/bg_<process>")
def bg_process(process):
	kwargs = request.args.to_dict()
	result = hook.Run("bg_" + process, **kwargs)
	return result
