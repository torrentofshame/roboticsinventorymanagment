from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_vue import Vue
from config import Config

db = SQLAlchemy()
login_manager = LoginManager()
vue = Vue()


def create_app():
	app = Flask(__name__, instance_relative_config=False)

	app.config.from_object(Config())

	db.init_app(app)
	login_manager.init_app(app)
	vue.init_app(app)

	with app.app_context():
		from . import routes
		from . import auth
		from . import cli

		app.register_blueprint(routes.main_bp)
		app.register_blueprint(auth.auth_bp)
		app.cli.add_command(cli.user_cli)

		db.create_all()

		return app
