from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField
from wtforms.validators import DataRequired, Email, EqualTo, Length


class SignupForm(FlaskForm):
    """User Signup Form."""
    name = StringField('Name',
                       validators=[DataRequired()])
    email = StringField('Email',
                        validators=[Length(min=6),
                                    Email(message='Enter a valid email.'),
                                    DataRequired()])
    password = PasswordField('Password',
                             validators=[DataRequired(),
                                         Length(min=6, message='Select a stronger password.')])
    confirm = PasswordField('Confirm Your Password',
                            validators=[DataRequired(),
                                        EqualTo('password', message='Passwords must match.')])
    submit = SubmitField('Register')

    def get(self, item):
        if item == "name":
            return self.name._value()
        elif item == "email":
            return self.email._value()
        elif item == "password":
            return self.password._value()
        elif item == "confirm":
            return self.confirm._value()

    def __getitem__(self, item):
        if item == "name":
            return self.name._value()
        elif item == "email":
            return self.email._value()
        elif item == "password":
            return self.password._value()
        elif item == "confirm":
            return self.confirm._value()

    def validate_on_submit(self):
        return self.is_submitted() and self.validate()

    def validate(self):
        for field in self._fields:
            if not self._fields[field]._value():
                return False
        if self._fields["password"]._value() != self._fields["password"]._value():
            return False
        return True


class LoginForm(FlaskForm):
    """User Login Form."""
    email = StringField('Email', validators=[DataRequired(),
                                             Email(message='Enter a valid email.')])
    password = PasswordField('Password', validators=[DataRequired()])
    submit = SubmitField('Log In')

    def get(self, item):
        if item == "email":
            return self.email._value()
        elif item == "password":
            return self.password._value()

    def __getitem__(self, item):
        if item == "email":
            return self.email._value()
        elif item == "password":
            return self.password._value()

    def validate_on_submit(self):
        return self.is_submitted() and self.validate()

    def validate(self):
        for field in self._fields:
            if not self._fields[field]._value():
                return False
        return True

class AddItem(FlaskForm):
    """Add Item Form."""
    pass
